Moth Eaten
==========
*Moth Eaten* is only provide that  pumpkin head be given wide viewing.

Maybe useable when the end expedition.

![screenshot](/uploads/21d9ec43e0c065ff3e77b1a386401a21/screenshot.png)

Usage
-----
Put *Moth-Eaten.zip* on /path/to/minecraft/resourcepacks

Please download from [latest pipeline](https://gitlab.com/yokotano/minecraft/resource-pack/moth-eaten/pipelines)

Build
-----
`docker-compoe run builder`
